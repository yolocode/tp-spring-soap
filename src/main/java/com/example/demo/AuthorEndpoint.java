package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bartradingx.GetAuthorRequest;
import com.bartradingx.GetAuthorResponse;
import com.example.demo.entities.Author;
import com.example.demo.repository.AuthorRepository;

@Endpoint
public class AuthorEndpoint {
 
    private static final String NAMESPACE_URI = "http://www.bartradingx.com";
 
    private AuthorRepository authorRepository;
 
    @Autowired
    public AuthorEndpoint(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }
 
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBookRequest")
    @ResponsePayload
    public GetAuthorResponse getCountry(@RequestPayload GetAuthorRequest request) {
        GetAuthorResponse response = new GetAuthorResponse();
        com.bartradingx.Author author = new com.bartradingx.Author();
        Author authorFromRepo = authorRepository.findByLastnameOrFirstname(request.getLastName(), request.getFisrtName());
        author.setId(authorFromRepo.getId());
        author.setFirstName(authorFromRepo.getFirstName());
        author.setLastName(authorFromRepo.getLastName());
        response.setAuthor(author);
 
        return response;
    }
}
