package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Author;
import com.example.demo.entities.Book;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {
	@Query("select a from Author a where a.firstName = :firstname or a.lastName = :lastname")
	Author findByLastnameOrFirstname(@Param("lastname") String lastname,
	                                 @Param("firstname") String firstname);
}