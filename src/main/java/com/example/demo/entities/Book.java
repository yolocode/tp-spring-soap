package com.example.demo.entities;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

@Entity
@XmlType(namespace = "https://localhost:8080/book")
public class Book {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private int id;
	@Column
	@XmlAttribute
	private String name;
	@Column
	@XmlAttribute
	private String isbn;
	@ManyToOne
	@XmlAttribute
	@JoinColumn(name = "forumId")
	private Author author;
	@Column
	@XmlAttribute
	@Temporal(TemporalType.DATE)
	private Date date;
	
	public Book(String name, String isbn, Author author, Date date) {
		this.name = name;
		this.isbn = isbn;
		this.author = author;
		this.date = date;
	}
	
	public String toString() {
		return this.name;
	}
	
	@XmlAttribute
	public int getId() {
      return id;
	}
	public void setId( int id ) {
      this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public XMLGregorianCalendar getXMLDate() {
		XMLGregorianCalendar xmlDate = null;
	    GregorianCalendar gc = new GregorianCalendar();
	    gc.setTime(date);
	 
	    try{
	      xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	    }catch(Exception e){
	      e.printStackTrace();
	    }
	    
	    return xmlDate;
	}

}
