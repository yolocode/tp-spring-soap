package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bartradingx.GetBookRequest;
import com.bartradingx.GetBookResponse;
import com.example.demo.entities.Book;
import com.example.demo.repository.BookRepository;


@Endpoint
public class BookEndpoint {
 
    private static final String NAMESPACE_URI = "http://www.bartradingx.com";
 
    private BookRepository bookRepository;
 
    @Autowired
    public BookEndpoint(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
 
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBookRequest")
    @ResponsePayload
    public GetBookResponse getCountry(@RequestPayload GetBookRequest request) {
        GetBookResponse response = new GetBookResponse();
        com.bartradingx.Book book = new com.bartradingx.Book();
        Book bookFromRepo = bookRepository.findByName(request.getName());
        book.setId(bookFromRepo.getId());
        book.setName(bookFromRepo.getName());
        book.setIsbn(bookFromRepo.getIsbn());
        book.setAuthorId(bookFromRepo.getAuthor().getId());
        book.setDate(bookFromRepo.getXMLDate());
        response.setBook(book);
 
        return response;
    }
}