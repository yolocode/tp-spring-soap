package com.example.demo;

import java.util.Date;
import java.util.List;

import javax.xml.ws.Endpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.entities.Author;
import com.example.demo.entities.Book;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		
		//Enpoint.publish("http://localhost:8080/ws", new AuthorImpl());
		
		/*AuthorService authServ = new AuthorService();
		BookService bookServ = new BookService();
		
		Author author1 = new Author("Pierre", "Chéné");
		Book book1 = new Book("Bonjour les gueux", "2-7654-1005-4", author1, new Date());
		
		authServ.save(author1);
		bookServ.save(book1);
		
		List<Author> auths = authServ.findAll();
		List<Book> books = bookServ.findAll();
		
		System.out.println(auths);
		System.out.println(books);*/
	}

}
